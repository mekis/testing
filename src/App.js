import React from "react";
import './App.css';
import {Login} from "./Login";
import {Register} from "./Register";
import {BrowserRouter,Routes,Route} from "react-router-dom";
import {Test} from "./test";
import {Unregister} from "./Unregister";
import { Needhelp } from "./Needhelp";

function App() {
  //const[currentForm, setCurrentForm]=useState('Login');
  
  return (
    <div className="App">
     <BrowserRouter>
      <Routes>
        <Route exact path="/" element={<Login/>}/>
        <Route exact path="/Register" element={<Register/>}/>
        <Route exact path="/test" element={<Test/>}/>
        <Route exact path="/unregister" element={<Unregister/>}/>
        <Route exact path="/needhelp" element={<Needhelp/>}/>
      </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
