import {useNavigate} from "react-router-dom";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

export const Needhelp=(props)=>{
    const navigate = useNavigate()

    const handleSubmit = (e) => {
       }
            return (
                    <Form>
                        <Form.Group>
                            <Form.Label name="label">The problem:</Form.Label>
                            <Form.Control placeholder = "Write the main problem" type="text"/>
                            <Form.Label>Description of the problem:</Form.Label>
                            <Form.Control placeholder = "Write longer description" type="text" ></Form.Control>
                        </Form.Group>
                        <Button name="needhelp" type= "submit" onSubmit={handleSubmit}>Send</Button>
                        <Button name="nazaj" className="link-btn" onClick={()=>navigate("/test")}>Back</Button>
                    </Form>
                );
            }
  